﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeDirectory.Web.API
{
    public class SearchQuery
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobID { get; set; }
        public string OfficeID { get; set; }
        public bool isActive { get; set; }
        public bool isArchived { get; set; }
    }
}