﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.Web.API
{
    public class ResultMessage
    {
        public string Result { get; set; }
    }
}
