﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeDirectory.Web.API.Models
{
    public class Categories
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
