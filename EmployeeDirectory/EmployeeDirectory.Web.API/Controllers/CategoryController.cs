﻿using EmployeeDirectory.BAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EmployeeDirectory.Web.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoryController : ApiController
    {
        [HttpPost, ActionName("Exists")]
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            var result = request.Content.ReadAsStringAsync().Result;
            var text = new Category().CategoryExist(result);

            var mm = new ResultMessage() { Result = text.ToString()};

            var answer =  new HttpResponseMessage() {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(mm))
            };

            answer.Content.Headers.Expires = DateTime.Now.AddMinutes(30);

            
            //answer.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue;
            //answer.Content.Headers.Add("Date", DateTime.Now.ToLongDateString());
            //answer.Content.Headers.Add("Pragma", "no-cache");

            return answer;
        }

        [HttpPost, ActionName("SaveCategory")]
        public HttpResponseMessage Save(HttpRequestMessage obj)
        {
            string objType = "Category";
            var result = obj.Content.ReadAsStringAsync().Result;

            Category cat = JsonConvert.DeserializeObject<Category>(result);

            var id = cat.Save(cat);
            cat.ID = (int)id;

            var answer = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.Created,
                Content = new StringContent(JsonConvert.SerializeObject(new { 
                    ID = cat.ID,
                    Name = cat.Name,
                    StatusCode = HttpStatusCode.Created.ToString(),
                    CreatedDate = DateTime.Now
                }))
            };
            return answer;
            //if (obj != null)
            //{

            //    if (!ModelState.IsValid)
            //    {
            //        return Request.CreateResponse(HttpStatusCode.NotAcceptable, string.Format("Your {0} object isn't valid", objType));
            //    }
            //    else
            //    {
            //        new Category().Save(obj);
            //        return Request.CreateResponse(HttpStatusCode.Created, obj);
            //    }
            //}
            //return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
