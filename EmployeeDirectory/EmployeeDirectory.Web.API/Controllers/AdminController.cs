﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmployeeDirectory.Web.API.Models;
using EmployeeDirectory.BAL;
using System.Web.Http.Cors;

namespace EmployeeDirectory.Web.API.Controllers
{
   [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminController : ApiController
    {
        #region Categories
        [HttpGet, ActionName("Categories")]
        public List<Category> GetAllCategories()
        {
           return new Category().GetAllCategories();
        }

        [HttpPost, ActionName("SaveCategory")]
        public HttpResponseMessage Save([FromBody] Category obj)
        {
            string objType = "Category";
            if (obj != null)
            {

                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, string.Format("Your {0} object isn't valid", objType));
                }
                else
                {
                    new Category().Save(obj);
                    return Request.CreateResponse(HttpStatusCode.Created, obj);
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
        #endregion

        #region SubCategories

        [HttpGet, ActionName("GetSubCategories")]
        public List<SubCategory> GetAllSubCategories()
        {
            return new SubCategory().GetAllSubCategories();
        }

        [HttpGet, ActionName("GetSubCategories")]
        public List<SubCategory> GetSubCategoriesByCategoryID([FromUri] int catID)
        {
            return new SubCategory().GetSubCategoriesByCategoryID(catID);
        }

        [HttpGet, ActionName("GetSubCategories")]
        public List<SubCategory> GetSubCategoriesByCategoryName([FromUri] string catName)
        {
            return new SubCategory().GetSubCategoriesByCategoryName(catName);
        }

        [HttpPost, ActionName("SaveSubCategory")]
        public HttpResponseMessage Save([FromBody] SubCategory obj)
        {
            string objType = "SubCategory";
            if (obj != null)
            {

                if (!ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, string.Format("Your {0} object isn't valid", objType));
                }
                else
                {
                    new SubCategory().Save(obj);
                    return Request.CreateResponse(HttpStatusCode.Created, obj);
                }
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
#endregion

    }
}
