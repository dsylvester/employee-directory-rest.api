﻿using EmployeeDirectory.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using EmployeeDirectory.BAL;
using Newtonsoft.Json;
using EmployeeDirectory.DAL.BaseClasses;
using Newtonsoft.Json.Linq;


namespace EmployeeDirectory.Web.API.Controllers
{
     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmployeeController : ApiController
    {
        
        [HttpGet, ActionName("GetEmployees")]
        public List<Employee> GetAllEmployees()
        {
            return new Employee().GetAllEmployee();
        }

        [HttpPost, ActionName("SearchEmployees")]
        public List<Employee> SearchAllEmployees(HttpRequestMessage obj)
        {
            var result = obj.Content.ReadAsStringAsync().Result;
            SearchQuery query = JsonConvert.DeserializeObject<SearchQuery>(result);

            return new Employee().SearchAllEmployeesByNameAndOfficeAndJob(query.FirstName, query.LastName, query.OfficeID, query.JobID, query.isActive, query.isArchived);
        }

        [HttpGet, ActionName("GetEmployee")]
        public List<Employee> GetEmployee([FromUri] int id)
        {
            return new Employee().GetEmployeeByID(id);
        }

        [HttpGet, ActionName("GetEmployee")]
        public List<Employee> GetAllEmployees([FromUri] string firstName, string lastName)
        {
            return new Employee().GetEmployeeByName(firstName, lastName);
        }
         
        //[HttpPost, ActionName("SaveEmployee")]
        //public HttpResponseMessage Save([FromBody] Employee newEmployee)
        //{
        //    if (newEmployee != null) {

        //        if (!ModelState.IsValid)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Your Employee object isn't valid");
        //        }
        //        else
        //        {
        //            new Employee().Save(newEmployee);
        //            return Request.CreateResponse(HttpStatusCode.Created, newEmployee);
        //        }
        //    }
        //    return Request.CreateResponse(HttpStatusCode.BadRequest);
        //}

         [HttpPost, ActionName("SaveEmployee")]
        public HttpResponseMessage Save(HttpRequestMessage obj)
        {
            string objType = "SaveEmployee";
            var result = obj.Content.ReadAsStringAsync().Result;

            Employee data = JsonConvert.DeserializeObject<Employee>(result);

            var id = data.Save(data);            

            if (id == null || id > 0 || data.ID > 0)
            {
                var answer = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.Created,
                    Content = new StringContent(JsonConvert.SerializeObject(new
                    {
                        ID = data.ID,
                        FullName = data.FirstName + " " + data.LastName,
                        StatusCode = HttpStatusCode.Created.ToString(),
                        CreatedDate = DateTime.Now,
                        StatusMessage = "Successfully Added!"
                    }))
                };
                return answer;
            }
              var failedResult = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent(JsonConvert.SerializeObject(new
                    {
                        ErrorMessage = HttpStatusCode.BadRequest.ToString(),
                        ErrorCode = HttpStatusCode.BadRequest
                    }))
                };
                return failedResult;
        }

    }
}
