﻿using EmployeeDirectory.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EmployeeDirectory.Web.API.Controllers
{

     [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {

         [HttpPost, ActionName("Save")]
         public HttpResponseMessage Post([FromBody] UserAccount obj)
         {
             string objType = "Category";
             if (obj != null)
             {

                 if (!ModelState.IsValid)
                 {
                     return Request.CreateResponse(HttpStatusCode.NotAcceptable, string.Format("Your {0} object isn't valid", objType));
                 }
                 else
                 {
                     //new UserAccount().Save(obj);
                     return Request.CreateResponse(HttpStatusCode.Created, obj);
                 }
             }
             return Request.CreateResponse(HttpStatusCode.BadRequest);
         }

        
    }
}
