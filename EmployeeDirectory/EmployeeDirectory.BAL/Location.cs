﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.BAL
{
    public class OfficeLocation
    {
        public Int64 ID { get; set; }
        public string Name { get; set; }
       
    }
}
