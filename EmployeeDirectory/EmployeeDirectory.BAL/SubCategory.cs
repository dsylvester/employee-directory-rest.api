﻿using EmployeeDirectory.DAL.BaseClasses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.BAL
{
    public class SubCategory
    {
        public int ID { get; set; } 
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public string Code { get; set; }

        [JsonProperty(PropertyName = "ShortDesc")]
        public string ShortDescription { get; set; }

        [JsonProperty(PropertyName = "LongDesc")]
        public string LongDescription { get; set; }

        public List<SubCategory> GetAllSubCategories()
        {
            return MapToObject(new SubCategoryData().GetAllSubCategories());
        }
        public List<SubCategory> GetSubCategoriesByCategoryID(int id)
        {
            return MapToObject(new SubCategoryData().GetSubCategoriesByCategoryID(id));
        }
        public List<SubCategory> GetSubCategoriesByID(int id)
        {
            return MapToObject(new SubCategoryData().GetSubCategoriesByID(id));
        }
        public List<SubCategory> GetSubCategoriesByCategoryName(string name)
        {
            return MapToObject(new SubCategoryData().GetSubCategoriesByCategoryName(name));
        }

        public int? Save(SubCategory data)
        {
            return new SubCategoryData().Save(MapToSubCategoryData(data));
        }

        public SubCategoryData MapToSubCategoryData(SubCategory data) {
            var temp = new SubCategoryData();
            temp.ID = data.ID;
            temp.CategoryID = data.CategoryID;
            temp.Code = data.Code;
            temp.Name = data.Name;
            temp.ShortDescription = data.ShortDescription;
            temp.LongDescription = data.LongDescription;

            return temp;


        }
        public List<SubCategory> MapToObject(List<SubCategoryData> data)
        {
            var result = new List<SubCategory>();
            data.ForEach(x =>
                result.Add(new SubCategory() {
                    ID = x.ID,
                    Name = x.Name,
                    CategoryID = x.CategoryID,
                    Code = x.Code,
                    ShortDescription = x.ShortDescription,
                    LongDescription = x.LongDescription
                })
                );
            return result;
        }
    }
}
