﻿using EmployeeDirectory.DAL;
using EmployeeDirectory.DAL.BaseClasses;
using SylvesterLLC.Utilities.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.BAL
{
    public class UserAccount
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string EMail { get; set; }

        [Required]
        public string Password { get; set; }

        public bool isActive { get; set; }
        public bool isArchived { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }

        public UserAccount() {
            SetDefaults();
        }

        public int? Save(UserAccount obj) {
            return new UserAccountData().Save(MapToObject(obj));
        }

        private UserAccountData MapToObject(UserAccount obj) {
            
            var data = new UserAccountData();
           
            data.ID = obj.ID;
            data.FirstName = obj.FirstName;
            data.LastName = obj.LastName;
            data.EMail = obj.EMail;
            data.Password = obj.Password;
            data.isActive = obj.isActive;
            data.isArchived = obj.isArchived;
            data.CreatedOn = obj.CreatedOn;
            data.CreatedBy = obj.CreatedBy;
            data.LastModifiedOn = obj.LastModifiedOn;
            data.LastModifiedBy = obj.LastModifiedBy;

            return data;
        }
        private void SetDefaults()
        {
            ID = -1;
            FirstName = string.Empty;
            LastName = string.Empty;
            EMail  = string.Empty;
            Password = string.Empty;
            isActive = true;
            isArchived = false;
            CreatedBy = GenericHelper.ReadConfigurationValue("SystemUserID").ToInt();
            CreatedOn = DateTime.Now;
            LastModifiedBy = GenericHelper.ReadConfigurationValue("SystemUserID").ToInt();
            LastModifiedOn = DateTime.Now;
        }


    }
}
