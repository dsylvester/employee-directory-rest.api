﻿using EmployeeDirectory.DAL.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.BAL
{
    public class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public List<Category> GetAllCategories() {
            return MapToObject(new CategoryData().GetAllCategories());
        }
        public bool CategoryExist(string name)
        {
            return new CategoryData().CategoryExist(name);
        }

        public int? Save(Category data)
        {
            return new CategoryData().Save(MapToCategoryData(data));
        }

        public CategoryData MapToCategoryData(Category data)
        {
            var temp = new CategoryData();
            temp.ID = data.ID;
            temp.Name = data.Name;
            
            return temp;
        }
        public List<Category> MapToObject(List<CategoryData> cd)
        { 
            var result = new List<Category>();
            cd.ForEach(x =>
                result.Add(new Category() { ID = x.ID, Name = x.Name})
                );
            return result;
        }
    }
}
