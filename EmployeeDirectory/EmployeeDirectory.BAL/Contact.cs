﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.BAL
{
    public class Contact
    {
        public int AreaCode { get; set; }
        public int Number { get; set; }
        public int Extension { get; set; }
       
    }
}
