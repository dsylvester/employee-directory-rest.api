﻿using EmployeeDirectory.DAL.BaseClasses;
using EmployeeDirectory.DAL.Interfaces;
using SylvesterLLC.Utilities.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.BAL.BaseClasses
{
    public abstract class Person : IHistory, IRecord, IArchival
    {
        
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string MiddleName { get; set; }
        
        [Required]
        public string Email { get; set; }
        
        public int Suffix { get; set; }
        
        public int CreatedBy {get; set;}
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        
        public int ID{get; set;}

        [Required]
        public bool isActive { get; set; }

        [Required]
        public bool isArchived { get; set; }

        public Person() {
           // SetDefaults();
        }

        private void SetDefaults() {
            ID = -1;
            MiddleName = string.Empty;
            Suffix = -1;
            CreatedBy = GenericHelper.ReadConfigurationValue("SystemUserID").ToInt();
            CreatedOn = DateTime.Now;
            LastModifiedBy = GenericHelper.ReadConfigurationValue("SystemUserID").ToInt();
            LastModifiedOn = DateTime.Now;
        }
       
    }
}
