﻿using EmployeeDirectory.BAL.BaseClasses;
using EmployeeDirectory.DAL.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ComponentModel.DataAnnotations;
using SylvesterLLC.Utilities.Generic;
using Newtonsoft.Json;

namespace EmployeeDirectory.BAL
{
    public class Employee : Person
    {
        [Required]
        [JsonProperty(PropertyName = "JobTitleID")]
        public int JobTitle { get; set; }

        [Required]
        [JsonProperty(PropertyName = "OfficeLocationID")]
        public int OfficeLocation { get; set; }

        [Required]
        public int? PersonID { get; set; }

        public int EmployeeID { get; set; }

        public string PhotoSrc { get; set; }

        public string OfficeLocationName { get; set; }

        public string JobTitleName { get; set; }

        public Employee()
        {
           // SetDefaults();
        }
        public List<Employee> GetAllEmployee()
        {
            return MapToObject(new EmployeeData().GetAll());
        }
        public List<Employee> SearchAllEmployeesByNameAndOfficeAndJob(string firstName = "", string lastName = "", string officeID = "", string jobID = "", bool isActive = true, bool isArchived = false)
        {
            return MapToObject(new EmployeeData().SearchEmployeeByNameOfficeJob(firstName, lastName, officeID, jobID, isActive, isArchived));
        }
        public List<Employee> GetEmployeeByName(string firstName = "", string lastName = "")
        {
            return MapToObject(new EmployeeData().GetEmployeeByName(firstName, lastName));
        }
        public List<Employee> GetEmployeeByID(int id)
        {
            return MapToObject(new EmployeeData().GetEmployeeByID(id));
        }

        public int? Save(Employee data)
        {
            return new EmployeeData().SavePerson(MapToEmployeeData(data));
        }

        public List<Employee> MapToObject(List<EmployeeData> data)
        {
            var result = new List<Employee>();
            data.ForEach(x =>
                result.Add(

                new Employee()
                {
                    ID = x.ID,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    MiddleName = x.MiddleName,
                    Email = x.Email,
                    Suffix = x.Suffix,
                    JobTitle = x.JobTitleID,
                    OfficeLocation = x.OfficeLocation,
                    PersonID = x.PersonID,
                    isActive = x.isActive,
                    isArchived = x.isArchived,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    LastModifiedBy = x.LastModifiedBy,
                    LastModifiedOn = x.LastModifiedOn,
                    EmployeeID = x.EmployeeID,
                    PhotoSrc = x.PhotoSrc,
                    OfficeLocationName = x.OfficeLocationName,
                    JobTitleName = x.JobTitleName
                }

                )
                );
            return result;
        }
        public EmployeeData MapToEmployeeData(Employee data)
        {
            var result = new EmployeeData();
            result.ID = data.ID;
            result.FirstName = data.FirstName;
            result.LastName = data.LastName;
            result.MiddleName = data.MiddleName;
            result.Email = data.Email;
            result.Suffix = data.Suffix;
            result.JobTitleID = data.JobTitle;
            result.OfficeLocation = data.OfficeLocation;
            result.PersonID = data.PersonID;
            result.CreatedBy = data.CreatedBy;
            result.CreatedOn = data.CreatedOn;
            result.LastModifiedBy = data.LastModifiedBy;
            result.LastModifiedOn = data.LastModifiedOn;
            result.EmployeeID = data.EmployeeID;
            result.isActive = data.isActive;
            result.isArchived = data.isArchived;
            result.PhotoSrc = data.PhotoSrc;
            result.OfficeLocationName = data.OfficeLocationName;
            result.JobTitleName = data.JobTitleName;
            return result;
        }
        public void SetDefaults()
        {
            JobTitle = -1;
            OfficeLocation = -1;
            PersonID = -1;
            EmployeeID = -1;
            OfficeLocationName = string.Empty;
            JobTitleName = string.Empty;
            PhotoSrc = GenericHelper.ReadConfigurationValue("DefaultPhotoSrc");
        }

    }
}
