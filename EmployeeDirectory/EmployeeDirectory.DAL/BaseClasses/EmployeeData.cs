﻿using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace EmployeeDirectory.DAL.BaseClasses
{
    public class EmployeeData : PersonData
    {

        public int ID { get; set; }
        public int JobTitleID { get; set; }
        public int OfficeLocation { get; set; }
        public int? PersonID { get; set; }
        public int EmployeeID { get; set; }
        public string PhotoSrc { get; set; }
        public string OfficeLocationName { get; set; }
        public string JobTitleName { get; set; }    
   
        DBContext db = new DBContext();

        public List<EmployeeData> GetAll()
        {
            
          return MapDataTableToObject(db.Select(StoredProcedure.GetAllEmployeeWithPerson()));
             
        }
        public List<EmployeeData> GetEmployeeByID(int id)
        {
            return MapDataTableToObject(db.Select(StoredProcedure.GetEmployeeWithPersonByID(id)));            
            
        }
        public List<EmployeeData> GetEmployeeByName(string firstName = "", string lastName = "")
        {
           return MapDataTableToObject(db.Select(StoredProcedure.GetEmployeeWithPersonByName(firstName, lastName)));            
           
        }
        public List<EmployeeData> SearchEmployeeByNameOfficeJob(string firstName = "", string lastName = "", string officeID = "", string jobID = "", bool isActive = true, bool isArchived = false){
           return MapDataTableToObject(db.Select(StoredProcedure.SearchEmployeesByNameAndOfficeAndLocation(firstName, lastName, officeID, jobID, isActive, isArchived)));
           
        }
        
        public int? SavePerson(EmployeeData data) {
            
            int? empID;
            int? dt = db.Save(StoredProcedure.SavePerson(data));
           
            if (dt == null) { 
                empID = SaveEmployee(data);
                return null;
            }
                data.PersonID = dt;
                empID = SaveEmployee(data);
                return dt;
           
            
            
        }

        public int? SaveEmployee(EmployeeData data) {
            return db.Save(StoredProcedure.SaveEmployee(data));
            
        }
        protected List<EmployeeData> MapDataTableToObject(DataTable dt)
        {
            var result = new List<EmployeeData>();

            using (DataTableReader reader = dt.CreateDataReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var data = new EmployeeData();
                        data.ID = Convert.ToInt32(reader[0]);
                        data.FirstName = reader[1].ToString();
                        data.LastName = reader[2].ToString();
                        data.MiddleName = reader[3].ToString();
                        data.Email = reader[4].ToString();
                        data.Suffix = Convert.ToInt32(reader[5]);
                        data.isActive = Convert.ToBoolean(reader[6]);
                        data.isArchived = Convert.ToBoolean(reader[7]);
                        data.CreatedBy = reader.IsDBNull(8) ?  -1 : Convert.ToInt32(reader[8]);
                        data.CreatedOn = reader.IsDBNull(9) ?  DateTime.Now.AddYears(-100)  : Convert.ToDateTime(reader[9]);
                        data.LastModifiedBy = reader.IsDBNull(10) ? -1 : Convert.ToInt32(reader[10]);
                        data.LastModifiedOn = reader.IsDBNull(11) ? DateTime.Now.AddYears(-100) : Convert.ToDateTime(reader[11]);
                        data.EmployeeID = Convert.ToInt32(reader[12]);
                        data.JobTitleID = Convert.ToInt32(reader[13]);
                        data.OfficeLocation = Convert.ToInt32(reader[14]);
                        data.PersonID = Convert.ToInt32(reader[15]);
                        data.PhotoSrc = reader[16].ToString();
                        data.JobTitleName = reader[17].ToString();
                        data.OfficeLocationName = reader[18].ToString();
                        
                        result.Add(data);
                    }
                }
            }
            return result;
        }
        

        
    }
}
