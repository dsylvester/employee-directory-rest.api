﻿using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.BaseClasses
{
    public class CategoryData :  IRecord
    {
        public int ID { get; set; }
        public string Name { get; set; }     
  
        DBContext db = new DBContext();

        public List<CategoryData>GetAllCategories() {
           
            return MapToObject(db.Select(StoredProcedure.GetAllCategories()));
            
        }        
        public bool CategoryExist(string name) {
            return ReturnValue(db.Select(StoredProcedure.CategoryExists(name)));
           
            
        }
        public int? Save(CategoryData data)
        {
          return db.Save(StoredProcedure.SaveCategory(data));
           
        }

        private bool ReturnValue(DataTable dt) {
            
            var result = false;

            using (DataTableReader reader = dt.CreateDataReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                       result =  Convert.ToBoolean(reader[0]);                        
                    }
                }
            }

            return result;
        }
        private List<CategoryData> MapToObject(DataTable dt){
            var result = new List<CategoryData>();

            using (DataTableReader reader = dt.CreateDataReader()) {
                if (reader.HasRows) {
                    while (reader.Read()) { 
                        var cd = new CategoryData();

                        cd.ID = Convert.ToInt32(reader[0]);
                        cd.Name = reader[1].ToString();

                        result.Add(cd);
                    }
                }
            }
            return result;
        }
        
    }
}
