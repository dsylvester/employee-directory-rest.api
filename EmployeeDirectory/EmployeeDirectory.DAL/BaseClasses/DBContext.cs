﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EmployeeDirectory.DAL.BaseClasses
{
    public class DBContext
    {
        public SqlConnection DBConnection
        {
            get
            {
                return GetNewSQLConnection();
            }
        }

        public DBContext()
        {
            GetNewSQLConnection();
        }

        private SqlConnection GetNewSQLConnection()
        {
            try
            {
                SqlConnection conn = new SqlConnection(GetConnectionString());
                conn.Close();
                return conn;
            }
            catch
            {
                throw new Exception("Error Creating a new Database Connection");
            }

        }
        private string GetConnectionString()
        {
            try
            {
                return ConfigurationManager.AppSettings["DBConnectionString"].ToString();
            }
            catch (Exception)
            {
                throw new Exception("You must Enter a Connection String in App.config or Web.config");

            }

        }

        public int? Save(SqlCommand cmd)
        {
            
            DBConnection.Close();
            cmd.Connection = DBConnection;
            cmd.Connection.Close();
            try
            {
                SqlParameter outParam = new SqlParameter();
                outParam.ParameterName = "@ReturnID";
                outParam.SqlDbType = SqlDbType.Int;
                outParam.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(outParam);
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                cmd.Connection.Close();

                var data = cmd.Parameters["@ReturnID"].Value.ToString();

                if (string.IsNullOrEmpty(cmd.Parameters["@ReturnID"].Value.ToString())){ return null;}
                return Convert.ToInt32(cmd.Parameters["@ReturnID"].Value.ToString());

            }

            catch (SqlException se)
            {

            }

            catch (Exception ex)
            {

            }

            finally
            {
                DBConnection.Close();

                

            }
            return null;
        }
        public DataTable Select(SqlCommand cmd) {
            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
            {
                cmd.Connection = DBConnection;
                DataTable dt = new DataTable();
                sda.SelectCommand = cmd;
                try
                {
                    sda.Fill(dt); 
                }

                catch (Exception ex) { 
                
                }
                

                return dt;
            }
            return null;

            
        }
    }
}