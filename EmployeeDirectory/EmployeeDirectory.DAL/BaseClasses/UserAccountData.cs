﻿using EmployeeDirectory.DAL.BaseClasses;
using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.BaseClasses
{
    public class UserAccountData : IRecord, IArchival, IHistory
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }
        public bool isActive  { get; set; }
        public bool isArchived  { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy  { get; set; }
        public DateTime LastModifiedOn { get; set; }
        
        DBContext db = new DBContext();

        public List<UserAccountData> GetAll()        {
            
            return MapToObject(db.Select(StoredProcedure.GetAllUserAccounts()));
            
        }
        public List<UserAccountData> GetByID(int id)
        {
           return MapToObject(db.Select(StoredProcedure.GetUserAccountByID(id)));
            
        }
        public List<UserAccountData> GetByName(string firstName, string lastName)
        {
            return MapToObject(db.Select(StoredProcedure.GetUserAccountByName(firstName,lastName)));
            
        }
        
        public int? Save(UserAccountData data)
        {
           return  db.Save(StoredProcedure.SaveUserAccount(data));
            
        }

        private List<UserAccountData> MapToObject(DataTable dt) {
            var result = new List<UserAccountData>();
            using (DataTableReader reader = dt.CreateDataReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var data = new UserAccountData();

                        data.ID = Convert.ToInt32(reader[0]);
                        data.FirstName = reader[1].ToString();
                        data.LastName = reader[2].ToString();
                        data.EMail = reader[3].ToString();
                        data.Password = reader[4].ToString();
                        data.isActive = Convert.ToBoolean(reader[5]);
                        data.isArchived = Convert.ToBoolean(reader[6]);
                        data.CreatedOn = Convert.ToDateTime(reader[7]);
                        data.CreatedBy = Convert.ToInt32(reader[8]);
                        data.LastModifiedOn = Convert.ToDateTime(reader[9]);
                        data.LastModifiedBy = Convert.ToInt32(reader[10]);

                        result.Add(data);
                    }
                    
                }
                return result;
            }
        }
    }
}
