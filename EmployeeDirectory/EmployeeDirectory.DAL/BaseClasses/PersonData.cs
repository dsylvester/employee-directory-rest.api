﻿using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.BaseClasses
{
    public abstract class PersonData : IRecord, IArchival, IHistory
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public int Suffix { get; set; }

        public int CreatedBy {get; set;}
        public DateTime CreatedOn { get; set; }
        public int LastModifiedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }

        

        public bool isActive { get; set; }
        public bool isArchived { get; set; }
        
        DBContext db = new DBContext();

        public List<PersonData> GetAllPeople()
        {            
            return MapDataTableToObject(db.Select(StoredProcedure.GetAllPeople()));

            
        }

        protected List<PersonData> MapDataTableToObject(DataTable dt)
        {
            var result = new List<PersonData>();

            using (DataTableReader reader = dt.CreateDataReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var data = new EmployeeData();

                        data.ID = Convert.ToInt32(reader[0]);
                        data.FirstName = reader[1].ToString();
                        data.LastName = reader[2].ToString();
                        data.MiddleName = reader[3].ToString();
                        data.Email = reader[4].ToString();
                        data.Suffix = Convert.ToInt32(reader[5]);
                        data.isActive = Convert.ToBoolean(reader[6]);
                        data.isArchived = Convert.ToBoolean(reader[7]);
                        result.Add(data);
                    }
                }
            }
            return result;
        }

    }
}

