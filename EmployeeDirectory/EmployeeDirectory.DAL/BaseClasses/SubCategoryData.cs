﻿using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.BaseClasses
{
    public class SubCategoryData : IRecord
    {
        private long _id;

        public string Name { get; set; }
        public int CategoryID { get; set; }
        public string Code { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }

        public int ID { get; set;}
        
        DBContext db = new DBContext();
        
        public List<SubCategoryData> GetAllSubCategories()
        {
            
            return MapDataTableToSubCategory(db.Select(StoredProcedure.GetAllSubCategories()));

            
        }
        public List<SubCategoryData> GetSubCategoriesByCategoryID(int id)
        {
            return MapDataTableToSubCategory(db.Select(StoredProcedure.GetSubCategoriesByCategoryID(id)));
           
        }
        public List<SubCategoryData> GetSubCategoriesByID(int id)
        {
           return MapDataTableToSubCategory(db.Select(StoredProcedure.GetSubCategoriesByID(id)));
         
        }
        public List<SubCategoryData> GetSubCategoriesByCategoryName(string name)
        {
            return MapDataTableToSubCategory(db.Select(StoredProcedure.GetSubCategoriesByCategoryName(name)));
           
        }
        public int? Save(SubCategoryData data)  {
            
          return db.Save(StoredProcedure.SaveSubCategory(data));
           
        }
        protected List<SubCategoryData> MapDataTableToSubCategory(DataTable dt) {
            var result = new List<SubCategoryData>();

            using (DataTableReader reader = dt.CreateDataReader())
            {
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var data = new SubCategoryData();

                        data.ID = Convert.ToInt32(reader[0]);
                        data.CategoryID = Convert.ToInt32(reader[1]);
                        data.Name = reader[2].ToString();
                        data.Code = reader[3].ToString();
                        data.ShortDescription = reader[4].ToString();
                        data.LongDescription = reader[5].ToString();
                        result.Add(data);
                    }
                }
            }
            return result;
        }
    }
}
