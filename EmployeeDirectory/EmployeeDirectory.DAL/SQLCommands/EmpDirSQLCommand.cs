﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.SQLCommands
{
    public class EmpDirSQLCommand 
    {
        

       public static SqlCommand getCommand(string storedProcedureName)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = storedProcedureName;
            command.CommandTimeout = 30;
            return command;
        }

       
    }
}
