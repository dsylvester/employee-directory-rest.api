﻿using EmployeeDirectory.DAL.BaseClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.SQLCommands
{
    public static class StoredProcedure
    {
        
       
        public static SqlCommand SaveCategory(CategoryData data)
        {

            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SaveCategory";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", data.ID);
            command.Parameters.AddWithValue("@name", data.Name);
            
            return command;

        }
        public static SqlCommand GetAllCategories()
        {
            SqlCommand command = new SqlCommand();
            //command.Connection
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Category_GetAll";
            command.CommandTimeout = 30;

            return command;
        }
        public static SqlCommand GetCategoriesByID(int id)
        {
            SqlCommand command = new SqlCommand();            
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Category_GetByID";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", id);

            return command;
        }
        public static SqlCommand CategoryExists(string name)
        {
            SqlCommand command = new SqlCommand();            
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Category_Exists";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@name", name);

            return command;
        }

        
        public static SqlCommand SaveUserAccount(UserAccountData data)
        {

            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_UserAccount_Save";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", data.ID);
            command.Parameters.AddWithValue("@firstName", data.FirstName);
            command.Parameters.AddWithValue("@lastName", data.LastName);
            command.Parameters.AddWithValue("@email", data.EMail);
            command.Parameters.AddWithValue("@passwd", data.Password);
            command.Parameters.AddWithValue("@isActive", data.isActive);
            command.Parameters.AddWithValue("@isArchived", data.isArchived);
            command.Parameters.AddWithValue("@createdBy", data.CreatedBy);
            command.Parameters.AddWithValue("@createdOn", data.CreatedOn);
            command.Parameters.AddWithValue("@lastModifiedBy", data.LastModifiedBy);
            command.Parameters.AddWithValue("@lastModifiedOn", data.LastModifiedOn);


            return command;

        }
        public static SqlCommand GetAllUserAccounts()
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_UserAccount_GetAll";
            command.CommandTimeout = 30;

            return command;
        }
        public static SqlCommand GetUserAccountByID(int id)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_UserAccount_GetByID";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", id);

            return command;
        }
        public static SqlCommand GetUserAccountByName(string firstName = "", string lastName = "")
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_UserAccount_GetByName";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@firstName", firstName);
            command.Parameters.AddWithValue("@lastName", lastName);

            return command;
        }
        public static SqlCommand SearchEmployeesByNameAndOfficeAndLocation(string firstName = "", string lastName = "", string officeID = "", string jobID = "",  bool isActive = true, bool isArchived = false)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Employee_SearchAllEmployeeWithPerson";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@fName", firstName);
            command.Parameters.AddWithValue("@lName", lastName);
            command.Parameters.AddWithValue("@officeID", officeID);
            command.Parameters.AddWithValue("@jobID", jobID);
            command.Parameters.AddWithValue("@isActive",isActive);
            command.Parameters.AddWithValue("@isArchived", isArchived);

            return command;
        }
        

        public static SqlCommand GetAllSubCategories()
        {
            SqlCommand command = new SqlCommand();
            //command.Connection
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SubCategory_GetAll";
            command.CommandTimeout = 30;

            return command;
        }
        public static SqlCommand GetSubCategoriesByCategoryID(int catID)
        {
            SqlCommand command = new SqlCommand();
            //command.Connection
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SubCategory_GetSubCategoriesByCategoryID";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@catID", catID);

            return command;
        }
        public static SqlCommand GetSubCategoriesByID(int id)
        {
            SqlCommand command = new SqlCommand();
            //command.Connection
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SubCategory_GetSubCategoriesByID";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", id);

            return command;
        }
        public static SqlCommand GetSubCategoriesByCategoryName(string catName)
        {
            SqlCommand command = new SqlCommand();
            //command.Connection
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SubCategory_GetSubCategoriesByCategoryName";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@catName", catName);

            return command;
        }
        public static SqlCommand SaveSubCategory(SubCategoryData data)
                {

                    SqlCommand command = new SqlCommand();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandText = "sp_SaveSubCategory";
                    command.CommandTimeout = 30;

                    command.Parameters.AddWithValue("@id", data.ID);
                    command.Parameters.AddWithValue("@name", data.Name);
                    command.Parameters.AddWithValue("@catID", data.CategoryID);
                    command.Parameters.AddWithValue("@code", data.Code);
                    command.Parameters.AddWithValue("@shortDesc", data.ShortDescription);
                    command.Parameters.AddWithValue("@longDesc", data.LongDescription);

                    return command;

                }

        public static SqlCommand GetAllPeople()
        {
            SqlCommand command = new SqlCommand();
            //command.Connection
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Person_GetAll";
            command.CommandTimeout = 30;

            return command;
        }
        public static SqlCommand GetPersonByName(string firstName = "", string lastName = "")
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Person_GetPersonByName";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@firstName", firstName);
            command.Parameters.AddWithValue("@lastName", lastName);

            return command;
        }
         public static SqlCommand SavePerson(EmployeeData data)
        {

            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SavePerson";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", data.PersonID);
            command.Parameters.AddWithValue("@FirstName", data.FirstName);
            command.Parameters.AddWithValue("@LastName", data.LastName);
            command.Parameters.AddWithValue("@MiddleName", data.MiddleName);
            command.Parameters.AddWithValue("@Email", data.Email);
            command.Parameters.AddWithValue("@Suffix", data.Suffix);
            command.Parameters.AddWithValue("@isActive", data.isActive);
            command.Parameters.AddWithValue("@isArchived", data.isArchived);
            command.Parameters.AddWithValue("@createdBy", data.CreatedBy);
            command.Parameters.AddWithValue("@createdOn", data.CreatedOn);
            command.Parameters.AddWithValue("@lastModifiedBy", data.LastModifiedBy);
            command.Parameters.AddWithValue("@lastModifiedOn", data.LastModifiedOn);
            
            return command;

        }

        public static SqlCommand GetEmployeeWithPersonByName(string firstName = "", string lastName = "")
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Employee_GetEmployeeWithPersonByName";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@firstName", firstName);
            command.Parameters.AddWithValue("@lastName", lastName);

            return command;
        }        
        public static SqlCommand GetEmployeeWithPersonByID(int id)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Employee_GetEmployeeWithPersonByID";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", id);            

            return command;
        }
        public static SqlCommand GetAllEmployeeWithPerson()
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_Employee_GetAllEmployeeWithPerson";
            command.CommandTimeout = 30;

            return command;
        }
        public static SqlCommand SaveEmployee(EmployeeData data)
        {

            SqlCommand command = new SqlCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "sp_SaveEmployee";
            command.CommandTimeout = 30;

            command.Parameters.AddWithValue("@id", data.EmployeeID);
            command.Parameters.AddWithValue("@JobTitleID", data.JobTitleID);
            command.Parameters.AddWithValue("@OfficeLocationID", data.OfficeLocation);
            command.Parameters.AddWithValue("@PersonID", data.PersonID);
            command.Parameters.AddWithValue("@PhotoSrc", data.PhotoSrc);

            return command;

        }
    }
}
