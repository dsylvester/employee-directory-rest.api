﻿using EmployeeDirectory.DAL.BaseClasses;
using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.Repositories
{
    public class EmployeeRepository : IRepo<EmployeeData>
    {
        DBContext db = new DBContext();

        public List<EmployeeData> GetAll()
        {
            throw new NotImplementedException();
        }

        public EmployeeData GetByID(long id)
        {
            throw new NotImplementedException();
        }

        public Int64? Save(EmployeeData data) {
            
            return db.Save(StoredProcedure.SaveEmployee(data));
        }
        public Int64? SavePerson(EmployeeData data)
        {            
            return db.Save(StoredProcedure.SavePerson(data));
        }
        public Int64? Save(string data)      {          
            return -1;
        }
    }
}
