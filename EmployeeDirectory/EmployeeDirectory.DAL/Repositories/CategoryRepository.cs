﻿using EmployeeDirectory.DAL.BaseClasses;
using EmployeeDirectory.DAL.Interfaces;
using EmployeeDirectory.DAL.SQLCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.Repositories
{
    public class CategoryRepository : IRepo<CategoryData>
    {
        public List<CategoryData> GetAll()
        {
            return new CategoryData().GetAllCategories(); 
        }

        public CategoryData GetByID(long id)
        {
            throw new NotImplementedException();
        }

        public long? Save(CategoryData data)
        {
            throw new NotImplementedException();
        }

        public long? Save(string data)
        {
            throw new NotImplementedException();
        }
    }
}
