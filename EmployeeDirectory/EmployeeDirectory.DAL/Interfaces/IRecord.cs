﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmployeeDirectory.DAL.Interfaces
{
    public interface IRecord
    {
        int ID { get; set; }
        
    }
}
