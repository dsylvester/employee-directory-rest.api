﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.Interfaces
{
    public interface IArchival
    {
        bool isActive { get; set; }
        bool isArchived { get; set; }
    }
}
