﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.Interfaces
{
    public interface IHistory
    {
        int CreatedBy { get; set; }
        DateTime CreatedOn { get; set; }
        int LastModifiedBy { get; set; }
        DateTime LastModifiedOn { get; set; }

    }
}
