﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeDirectory.DAL.Interfaces
{
    public interface IRepo<T> where T: IRecord
    {
        List<T> GetAll();
        T GetByID(Int64 id);
        Int64? Save(T data);
        Int64? Save(string data);



    }
}
